#include <stdio.h>

int main (void)
{
    int i, n;
    int factorial = 1;

    for (i = 1; i <=10; ++i) {
        for (n = i; n > 0; --n)
            factorial = factorial * n;
        printf("%2i! = %i\n", i, factorial);
        factorial = 1;
    }

    return 0;
}