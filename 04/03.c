#include <stdio.h>

int main (void)
{
    int i, n;

    for (i = 1; i <=10; ++i) {
        n = 5 * i;
        printf("%2i -> %i\n", n, n * (n + 1) / 2);
    }

    return 0;
}